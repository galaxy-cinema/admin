export function calculateEndTime(selectedMovieDuration, startTime) {
  const durationMinutes = parseInt(selectedMovieDuration, 10);
  const durationHours = Math.floor(durationMinutes / 60);
  const remainingMinutes = durationMinutes % 60;

  const [startHours, startMinutes, startSeconds] = startTime
    .split(":")
    .map(Number);

  let endHours = startHours + durationHours;
  let endMinutes = startMinutes + remainingMinutes;
  let endSeconds = startSeconds;

  if (endMinutes >= 60) {
    endHours += Math.floor(endMinutes / 60);
    endMinutes = endMinutes % 60;
  }

  const formatTime = (time) => time.toString().padStart(2, "0");

  const endTime = `${formatTime(endHours)}:${formatTime(
    endMinutes
  )}:${formatTime(endSeconds)}`;

  return endTime;
}
