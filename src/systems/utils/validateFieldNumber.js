export const validateFieldNumber = (label, value) => {
  if (!value) {
    return Promise.reject(`Vui lòng nhập ${label}`);
  }
  if (!/^\d+$/.test(value)) {
    return Promise.reject(`Sai định dạng ${label}`);
  }
  return Promise.resolve();
};
