  import axios from "axios";
  import { jwtDecode } from "jwt-decode";

  const axiosAuthInstance = axios.create({
    baseURL: "http://localhost:2712/api/v1/admin",
    withCredentials: true,
  });

  const axiosNoAuthInstance = axios.create({
    baseURL: "http://localhost:2712/api/v1/admin",
    withCredentials: true,
  });

  let isRefreshing = false;
  let refreshPromise = null;

  const refreshToken = async () => {
    try {
      const response = await axiosNoAuthInstance.post("/refresh-token");
      return response.data;
    } catch (error) {
      throw error;
    }
  };

  axiosAuthInstance.interceptors.request.use(
    async (config) => {
      const token = localStorage.getItem("authToken");
      if (token) {
        const decodedToken = jwtDecode(token);
        const currentTime = Date.now() / 1000;

        if (decodedToken.exp < currentTime) {
          if (!isRefreshing) {
            isRefreshing = true;
            refreshPromise = refreshToken()
              .then((newToken) => {
                isRefreshing = false;
                localStorage.setItem("authToken", newToken.accessToken);
                return newToken.accessToken;
              })
              .catch((err) => {
                isRefreshing = false;
                localStorage.removeItem("authToken");
                throw err;
              });
          }
          const newToken = await refreshPromise;
          config.headers.Authorization = `Bearer ${newToken}`;
        } else {
          config.headers.Authorization = `Bearer ${token}`;
        }
      }
      return config;
    },
    (error) => {
      console.error("Interceptor error:", error);
      return Promise.reject(error);
    }
  );

  export const apiAuth = axiosAuthInstance;
  export const apiNoAuth = axiosNoAuthInstance;
