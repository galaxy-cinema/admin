import { useQuery } from "@tanstack/react-query";
import { getSeatsByRoom } from "../../services/seat";

const useSeatQuery = (id) => {
  const queryKey = ["SEAT_KEY", id];
  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      if (!id) return null;
      return await getSeatsByRoom(id);
    },
    enabled: !!id,
    staleTime: 60000,
    cacheTime: 300000,
  });
  return { data, ...rest };
};

export default useSeatQuery;
