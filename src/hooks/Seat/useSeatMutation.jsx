import { useMutation, useQueryClient } from "@tanstack/react-query";
import { update } from "../../services/seat";
const useSeatMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (seat) => {
      switch (action) {
        case "UPDATE":
          return await update(seat);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["SEAT_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useSeatMutation;
