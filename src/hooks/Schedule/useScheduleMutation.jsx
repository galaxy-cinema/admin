import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/schedule";
const useScheduleMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (schedule) => {
      switch (action) {
        case "CREATE":
          return await create(schedule);
        case "DELETE":
          return await remove(schedule);
        case "UPDATE":
          return await update(schedule);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["SCHEDULE_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useScheduleMutation;
