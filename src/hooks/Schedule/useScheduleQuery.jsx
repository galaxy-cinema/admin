import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../services/schedule";

const useScheduleQuery = (id) => {
  const queryKey = id ? ["SCHEDULE_KEY", id] : ["SCHEDULE_KEY"];

  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      return id ? await getById(id) : await get();
    },
    staleTime: 60000,
    cacheTime: 300000,
  });

  return { data, ...rest };
};

export default useScheduleQuery;
