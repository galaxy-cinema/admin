import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/cinema";
const useCinemaMutation = ({ action, onSuccess, onError, messageApi }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (cinema) => {
      switch (action) {
        case "CREATE":
          return await create(cinema);
        case "DELETE":
          return await remove(cinema);
        case "UPDATE":
          return await update(cinema);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["CINEMA_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useCinemaMutation;
