import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../services/movie";
const useMovieQuery = (id) => {
  const queryKey = id ? ["MOVIE_KEY", id] : ["MOVIE_KEY"];

  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      return id ? await getById(id) : await get();
    },
    staleTime: 60000,
    cacheTime: 300000,
  });

  return { data, ...rest };
};

export default useMovieQuery;
