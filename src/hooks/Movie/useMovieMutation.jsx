import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/movie";
const useMovieMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (movie) => {
      switch (action) {
        case "CREATE":
          return await create(movie);
        case "DELETE":
          return await remove(movie);
        case "UPDATE":
          return await update(movie);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["MOVIE_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useMovieMutation;
