import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../services/seatType";
const useSeatTypeQuery = (id) => {
  const queryKey = id ? ["SEAT_TYPE_KEY", id] : ["SEAT_TYPE_KEY"];
  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      return id ? await getById(id) : await get();
    },
    staleTime: 60000,
    cacheTime: 300000,
  });
  return { data, ...rest };
};
export default useSeatTypeQuery;
