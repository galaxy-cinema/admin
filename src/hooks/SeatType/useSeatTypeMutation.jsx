import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/seatType";
const useSeatTypeMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (seatType) => {
      switch (action) {
        case "CREATE":
          return await create(seatType);
        case "DELETE":
          return await remove(seatType);
        case "UPDATE":
          return await update(seatType);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["SEAT_TYPE_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useSeatTypeMutation;
