import { useQuery } from "@tanstack/react-query";
import { get, getById } from "../../services/genre";

const useGenreQuery = (id) => {
  const queryKey = id ? ["GENRE_KEY", id] : ["GENRE_KEY"];

  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      if (id) {
        return await getById(id);
      } else {
        return await get();
      }
    },
    staleTime: 60000,
    cacheTime: 300000,
  });

  return { data, ...rest };
};

export default useGenreQuery;
