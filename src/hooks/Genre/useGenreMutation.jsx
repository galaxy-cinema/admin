import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/genre";
const useGenreMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (genre) => {
      switch (action) {
        case "CREATE":
          return await create(genre);
        case "DELETE":
          return await remove(genre);
        case "UPDATE":
          return await update(genre);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["GENRE_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useGenreMutation;
