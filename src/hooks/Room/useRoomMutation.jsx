import { useMutation, useQueryClient } from "@tanstack/react-query";
import { create, remove, update } from "../../services/room";
const useRoomMutation = ({ action, onSuccess, onError }) => {
  const queryClient = useQueryClient();
  const { mutate, ...rest } = useMutation({
    mutationFn: async (room) => {
      switch (action) {
        case "CREATE":
          return await create(room);
        case "DELETE":
          return await remove(room);
        case "UPDATE":
          return await update(room);
        default:
          return null;
      }
    },
    onSuccess: () => {
      onSuccess && onSuccess();
      queryClient.invalidateQueries({
        queryKey: ["ROOM_KEY"],
      });
    },
    onError: (error) => {
      onError && onError(error);
    },
  });
  return { mutate, ...rest };
};

export default useRoomMutation;
