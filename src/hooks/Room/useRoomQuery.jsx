import { useQuery } from "@tanstack/react-query";
import { get, getRoomsByCinema } from "../../services/room";
const useRoomQuery = (id) => {
  const queryKey = id ? ["ROOM_KEY", id] : ["ROOM_KEY"];

  const { data, ...rest } = useQuery({
    queryKey,
    queryFn: async () => {
      return id ? await getRoomsByCinema(id) : await get();
    },
    staleTime: 60000,
    cacheTime: 300000,
  });

  return { data, ...rest };
};

export default useRoomQuery;
