import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get(`/genres`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getById = async (id) => {
  try {
    const { data } = await apiAuth.get(`/genres/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const create = async (genre) => {
  try {
    const { data } = await apiAuth.post("/genres", genre);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/genres/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const update = async (genre) => {
  console.log(genre);
  try {
    const { data } = await apiAuth.put(`/genres/${genre.id}`, genre);
    console.log(data);
    return data;
  } catch (error) {
    throw error;
  }
};
