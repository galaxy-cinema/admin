import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get(`/cinemas`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getById = async (id) => {
  try {
    const { data } = await apiAuth.get(`/cinemas/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const create = async (cinema) => {
  try {
    const { data } = await apiAuth.post("/cinemas", cinema);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/cinemas/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const update = async (cinema) => {
  console.log(cinema);
  try {
    const { data } = await apiAuth.put(`/cinemas/${cinema.id}`, cinema);
    console.log(data);
    return data;
  } catch (error) {
    throw error;
  }
};
