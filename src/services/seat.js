import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const getSeatsByRoom = async (id) => {
  try {
    const { data } = await apiAuth.get(`/seats/${id}`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const update = async (seats) => {
  try {
    console.log(seats);
    const { data } = await apiAuth.put("/seats", seats);
    return data;
  } catch (error) {
    throw error;
  }
};
