import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get(`/rooms`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getRoomsByCinema = async (id) => {
  try {
    const { data } = await apiAuth.get(`/rooms/${id}`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const create = async (room) => {
  try {
    const { data } = await apiAuth.post("/rooms", room);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/genres/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const update = async (room) => {
  console.log(room);
  try {
    const { data } = await apiAuth.put(`/rooms/${room.id}`, room);
    return data;
  } catch (error) {
    throw error;
  }
};
