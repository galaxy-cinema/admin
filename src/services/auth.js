import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const signIn = async (credentials, navigate) => {
  try {
    const { data } = await apiNoAuth.post("/sign-in", credentials);
    if (data.accessToken && data.accessToken.split(".").length === 3) {
      localStorage.setItem("authToken", data.accessToken);
    } else {
      console.error("Invalid token format, not saving to localStorage");
    }
    navigate("/");
    return data;
  } catch (error) {
    throw error;
  }
};
export const logout = async () => {
  try {
    const res = await apiNoAuth.post("/logout");
    return res;
  } catch (error) {
    throw error;
  }
};
export const refreshToken = async () => {
  try {
    const token = await apiAuth.post("/refresh-token");
    return token;
  } catch (error) {
    throw error;
  }
};

export const verifyToken = async (token) => {
  try {
    const res = await apiAuth.post("/verify-token", token);
    return res;
  } catch (error) {
    throw error;
  }
};
