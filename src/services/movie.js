import { apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get(`/movies`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getById = async (id) => {
  try {
    const { data } = await apiAuth.get(`/movies/${id}`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const create = async (movie) => {
  try {
    const { data } = await apiAuth.post("/movies", movie);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/movies/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const update = async (movie) => {
  console.log(movie);
  try {
    const { data } = await apiAuth.put(`/movies/${movie.id}`, movie);
    console.log(data);
    return data;
  } catch (error) {
    throw error;
  }
};
