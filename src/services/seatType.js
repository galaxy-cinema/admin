import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get(`/seat-types`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const getById = async (id) => {
  try {
    const { data } = await apiAuth.get(`/seat-types/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const create = async (seatType) => {
  try {
    const { data } = await apiAuth.post("/seat-types", seatType);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/seat-types/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};
export const update = async (seatType) => {
  console.log(seatType);
  try {
    const { data } = await apiAuth.put(`/seat-types/${seatType.id}`, seatType);
    return data;
  } catch (error) {
    throw error;
  }
};
