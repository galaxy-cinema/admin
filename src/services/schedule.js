import { apiNoAuth, apiAuth } from "../configs/axios.js";

export const get = async () => {
  try {
    const { data } = await apiAuth.get("/schedules");
    return data;
  } catch (error) {
    throw error;
  }
};

export const getById = async (id) => {
  try {
    const { data } = await apiAuth.get(`/schedules/${id}`);
    return data;
  } catch (error) {
    throw error;
  }
};

export const create = async (schedule) => {
  try {
    const { data } = await apiAuth.post("/schedules", schedule);
    return data;
  } catch (error) {
    throw error;
  }
};

export const remove = async (id) => {
  try {
    const { data } = await apiAuth.delete(`/schedules/${id}`);
    return data.data;
  } catch (error) {
    throw error;
  }
};

export const update = async (schedule) => {
  console.log(schedule);
  try {
    const { data } = await apiAuth.put(`/schedules/${schedule.id}`, schedule);
    console.log(data);
    return data;
  } catch (error) {
    throw error;
  }
};
