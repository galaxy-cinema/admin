import { Layout, theme } from "antd";
import React from "react";
import { Outlet } from "react-router-dom";
import Sidebar from "../components/common/Sidebar/Sidebar.jsx";
const { Header, Content } = Layout;
const BasicLayout = () => {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();
  return (
    <>
      <Layout className="h-screen">
        <Sidebar />
        <Layout>
          <Header
            style={{
              padding: 0,
              background: colorBgContainer,
            }}
          />
          <Content
            style={{
              margin: "24px 16px 0",
            }}
          >
            <div
              className="h-full"
              style={{
                padding: 24,
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
              }}
            >
              <Outlet />
            </div>
          </Content>
        </Layout>
      </Layout>
    </>
  );
};

export default BasicLayout;
