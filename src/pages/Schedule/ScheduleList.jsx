import { EditOutlined } from "@ant-design/icons";
import { Button, Table } from "antd";
import React, { useState } from "react";
import useScheduleQuery from "../../hooks/Schedule/useScheduleQuery";
import { formatDate } from "../../systems/utils/formatDate";
import ScheduleCreate from "./ScheduleCreate";
import ScheduleUpdate from "./ScheduleUpdate";
const columns = (openModelUpdate) => [
  {
    title: "Phim",
    dataIndex: "movie",
    key: "movie",
    render: (text) => <>{text.name}</>,
  },
  {
    title: "Phòng",
    dataIndex: "room",
    key: "room",
    render: (text) => <>{text.name}</>,
  },
  {
    title: "Ngày chiếu",
    dataIndex: "show_date",
    key: "show_date",
  },
  {
    title: "Thời gian chiếu phim ",
    dataIndex: "start_time",
    key: "start_time",
  },
  {
    title: "Thời gian kết thúc phim",
    dataIndex: "end_time",
    key: "end_time",
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "",
    key: "action",
    render: (_, cinema) => (
      <Button type="primary" ghost onClick={() => openModelUpdate(cinema)}>
        <EditOutlined />
      </Button>
    ),
  },
];

const ScheduleList = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useScheduleQuery();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedSchedule, setSelectedSchedule] = useState(null);
  const [modalType, setModalType] = useState(null);
  const dataSource = data?.schedules?.map((item) => ({
    key: item.id,
    ...item,
  }));
  console.log(dataSource);
  const showModal = () => {
    setIsModalOpen(true);
    setModalType("create");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setSelectedSchedule(null);
  };

  const openModelUpdate = (cinema) => {
    setSelectedSchedule(cinema);
    setIsModalOpen(true);
    setModalType("update");
  };

  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Danh sách lịch chiếu phim</h1>
        <Button type="primary" onClick={showModal}>
          Thêm lịch chiếu
        </Button>
      </div>
      {isModalOpen && modalType === "create" && (
        <ScheduleCreate open={isModalOpen} onCancel={handleCancel} />
      )}
      {isModalOpen && modalType === "update" && selectedSchedule && (
        <ScheduleUpdate
          open={isModalOpen}
          onCancel={handleCancel}
          schedule={selectedSchedule}
        />
      )}
      <Table
        columns={columns(openModelUpdate)}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default ScheduleList;
