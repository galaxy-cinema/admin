import {
  Button,
  Col,
  DatePicker,
  Form,
  message,
  Modal,
  Row,
  Select,
  TimePicker,
} from "antd";
import dayjs from "dayjs";
import React, { useEffect, useMemo, useState } from "react";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import useRoomQuery from "../../hooks/Room/useRoomQuery";
import useScheduleMutation from "../../hooks/Schedule/useScheduleMutation";

const ScheduleUpdate = ({ open, onCancel, schedule }) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();
  const [selectedMovieDuration, setSelectedMovieDuration] = useState(null);
  const { data: roomData, isLoading: loadingRoom } = useRoomQuery();
  const { data: movieData, isLoading: loadingMovie } = useMovieQuery();
  const [startTime, setStartTime] = useState();
  const [startDate, setStartDate] = useState();
  useEffect(() => {
    if (schedule) {
      form.setFieldsValue({
        ...schedule,
        show_date: dayjs(schedule.show_date, "YYYY-MM-DD"),
        start_time: dayjs(schedule.start_time, "HH:mm:ss"),
      });
    }
  }, [schedule, form]);
  const { mutate: create, isPending } = useScheduleMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Thêm rạp thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const roomOptions = useMemo(() => {
    if (!roomData || !roomData.rooms) return [];
    return roomData.rooms.map((room) => ({
      value: room.id,
      label: room.name,
    }));
  }, [roomData]);

  const movieOptions = useMemo(() => {
    if (!movieData || !movieData.movies) return [];
    return movieData.movies.map((room) => ({
      value: room.id,
      label: room.name,
    }));
  }, [movieData]);

  const handleMovieSelect = (movieId) => {
    const selectedMovie = movieData.movies.find(
      (movie) => movie.id === movieId
    );
    if (selectedMovie && selectedMovie.duration) {
      setSelectedMovieDuration(parseInt(selectedMovie.duration, 10));
    } else {
      setSelectedMovieDuration(null);
    }
  };

  const onFinish = (values) => {
    create({
      ...values,
      start_date: startDate,
      start_time: startTime,
      end_time: calculateEndTime(selectedMovieDuration, startTime),
    });
  };

  return (
    <>
      {contextHolder}
      <Modal
        title="Thêm rạp chiếu phim"
        open={open}
        onCancel={isPending ? null : onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel} disabled={isPending}>
            Hủy
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => form.submit()}
            loading={isPending}
            disabled={isPending}
          >
            {isPending ? "Đang thêm..." : "Thêm"}
          </Button>,
        ]}
        maskClosable={!isPending}
        closable={!isPending}
      >
        <div>
          <Form
            form={form}
            layout="vertical"
            name="basic"
            style={{
              maxWidth: "100%",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="Chọn phòng chiếu"
              name="room_id"
              rules={[{ required: true, message: "Vui lòng chọn phòng" }]}
            >
              <Select
                loading={loadingRoom}
                placeholder="Chọn rạp chiếu"
                style={{
                  width: "100%",
                }}
                options={roomOptions}
              />
            </Form.Item>
            <Form.Item
              label="Chọn phim chiếu"
              name="movie_id"
              rules={[{ required: true, message: "Vui lòng chọn phim" }]}
            >
              <Select
                loading={loadingMovie}
                placeholder="Chọn phim chiếu"
                style={{ width: "100%" }}
                options={movieOptions}
                onChange={handleMovieSelect}
              />
            </Form.Item>
            <Form.Item
              label="Ngày chiếu"
              name="show_date"
              rules={[
                { required: true, message: "Không được bỏ trống" },
                { type: "date", message: "Ngày không hợp lệ" },
              ]}
            >
              <DatePicker
                className="w-full"
                format="YYYY-MM-DD"
                onChange={(date, dateString) => setStartDate(dateString)}
                disabledDate={(current) => {
                  return current && current < dayjs().startOf("day");
                }}
              />
            </Form.Item>
            <Row gutter={16}>
              <Col span={12}>
                <Form.Item
                  label="Thời gian chiếu"
                  name="start_time"
                  rules={[{ required: true, message: "Không được bỏ trống" }]}
                >
                  <TimePicker
                    defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
                    onChange={(time, timeString) => {
                      setStartTime(timeString);
                    }}
                  />
                </Form.Item>
              </Col>
            </Row>
          </Form>
        </div>
      </Modal>
    </>
  );
};

export default ScheduleUpdate;
