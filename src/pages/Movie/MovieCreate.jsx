import {
  Form,
  Row,
  Col,
  Input,
  Select,
  DatePicker,
  Button,
  message,
  Upload,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import React, { useMemo, useState } from "react";
import useGenreQuery from "../../hooks/Genre/useGenreQuery";
import useMovieMutation from "../../hooks/Movie/useMovieMutation";
import dayjs from "dayjs";

const MovieCreate = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const { data, isLoading } = useGenreQuery();

  const { mutate, isPending } = useMovieMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      setFileList([]);
      messageApi.success("Thêm phim thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });
  const [endDate, setEndData] = useState(null);
  const [releaseDate, setReleaseData] = useState(null);

  const genreOptions = useMemo(() => {
    if (!data || !data.genres) return [];
    return data.genres.map((genre) => ({
      value: genre.id,
      label: genre.name,
    }));
  }, [data]);

  const [form] = Form.useForm();

  const [fileListLandscape, setFileListLandscape] = useState([]);
  const [fileListPortrait, setFileListPortrait] = useState([]);

  const onFinish = (values) => {
    if (fileListLandscape.length === 0 || fileListPortrait.length === 0) {
      messageApi.error("Vui lòng tải lên cả hai ảnh");
      return;
    }
    mutate({
      ...values,
      release_date: releaseDate,
      end_date: endDate,
      image_landscape: fileListLandscape[0].thumbUrl,
      image_portrait: fileListPortrait[0].thumbUrl,
    });
  };
  return (
    <div>
      {contextHolder}
      <Form
        form={form}
        layout="vertical"
        name="basic"
        style={{
          maxWidth: "100%",
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="on"
      >
        <Row gutter={16}>
          <Col span={18}>
            <Form.Item
              label="Nhập tên phim"
              name="name"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Thời lượng phim"
              name="duration"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={18}>
            <Form.Item
              label="Trailer"
              name="trailer"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Giới hạn độ tuổi"
              name="age_restriction"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={18}>
            <Form.Item
              label="Thể loại"
              name="genre_id"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Select
                mode="multiple"
                allowClear
                style={{
                  width: "100%",
                }}
                placeholder="Please select"
                options={genreOptions}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Giá"
              name="price"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Tác giả"
              name="director"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Diễn viên"
              name="actor"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item
              label="Ngày công chiếu"
              name="release_date"
              rules={[
                { required: true, message: "Không được bỏ trống" },
                { type: "date", message: "Ngày không hợp lệ" },
              ]}
            >
              <DatePicker
                className="w-full"
                format="YYYY-MM-DD"
                disabledDate={(current) => {
                  return current && current < dayjs().startOf("day");
                }}
                onChange={(date, dateString) => {
                  setReleaseData(dateString);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item
              label="Ngày dừng chiếu"
              name="end_date"
              rules={[
                { required: true, message: "Không được bỏ trống" },
                { type: "date", message: "Ngày không hợp lệ" },
              ]}
            >
              <DatePicker
                className="w-full"
                format="YYYY-MM-DD"
                disabledDate={(current) => {
                  return current && current < dayjs().startOf("day");
                }}
                onChange={(date, dateString) => {
                  setEndData(dateString);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="image_landscape"
              label="Ảnh ngang"
              rules={[
                { required: true, message: "Vui lòng tải lên ảnh ngang" },
              ]}
            >
              <Upload
                listType="picture"
                fileList={fileListLandscape}
                beforeUpload={() => false}
                onChange={({ fileList }) => {
                  setFileListLandscape(fileList.slice(0, 1));
                }}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>Tải ảnh ngang</Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="image_portrait"
              label="Ảnh dọc"
              rules={[{ required: true, message: "Vui lòng tải lên ảnh dọc" }]}
            >
              <Upload
                listType="picture"
                fileList={fileListPortrait}
                beforeUpload={() => false}
                onChange={({ fileList }) => {
                  setFileListPortrait(fileList.slice(0, 1));
                }}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>Tải ảnh dọc</Button>
              </Upload>
            </Form.Item>
          </Col>
        </Row>
        <Button key="submit" type="primary" onClick={() => form.submit()}>
          Thênm
        </Button>
      </Form>
    </div>
  );
};

export default MovieCreate;
