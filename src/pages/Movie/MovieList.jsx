import { Table, Tag, message } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import { formatDate } from "../../systems/utils/formatDate";

const columns = [
  {
    title: "Tên phim",
    dataIndex: "name",
    key: "name",
    render: (_, movie) => <Link to={`/movies/${movie.id}`}>{movie.name}</Link>,
  },
  {
    title: "Đạo diễn",
    dataIndex: "actor",
    key: "actor",
  },
  {
    title: "Thể loại",
    dataIndex: "genres",
    key: "genres",
    render: (_, { genres }) => (
      <>
        {genres.map((genre) => {
          return (
            <Tag color="geekblue" key={genre.name}>
              {genre.name}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: "Ngày công chiếu",
    dataIndex: "release_date",
    key: "release_date",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày kết thúc",
    dataIndex: "end_date",
    key: "end_date",
    render: (date) => formatDate(date),
  },
  {
    title: "Trạng thái",
    dataIndex: "status",
    key: "status",
    render: (_, { status }) => {
      let color;
      if (status === "ended") {
        color = "volcano";
      } else if (status === "now_showing") {
        color = "green";
      } else if (status === "upcoming") {
        color = "cyan";
      }

      return (
        <Tag className="capitalize" color={color}>
          {status}
        </Tag>
      );
    },
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày chỉnh sửa",
    dataIndex: "updatedAt",
    key: "updatedAt",
    render: (date) => formatDate(date),
  },
];

const MovieList = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useMovieQuery();

  const dataSource = data?.movies?.map((item) => ({
    key: item.id,
    ...item,
  }));
  console.log(dataSource);
  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      {contextHolder}
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Danh sách phim</h1>
      </div>

      <Table
        columns={columns}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default MovieList;
