import {
  Form,
  Row,
  Col,
  Input,
  Select,
  DatePicker,
  Button,
  message,
  Upload,
} from "antd";
import { UploadOutlined } from "@ant-design/icons";
import React, { useEffect, useMemo, useState } from "react";
import useGenreQuery from "../../hooks/Genre/useGenreQuery";
import useMovieQuery from "../../hooks/Movie/useMovieQuery";
import useMovieMutation from "../../hooks/Movie/useMovieMutation";
import { useParams } from "react-router-dom";
import moment from "moment";
import dayjs from "dayjs";

const MovieUpdate = () => {
  const { id } = useParams();
  const [form] = Form.useForm();
  const [messageApi, contextHolder] = message.useMessage();

  const { data: movieData, isLoading } = useMovieQuery(id);
  const { data } = useGenreQuery();

  const [endDate, setEndData] = useState(null);
  const [releaseDate, setReleaseData] = useState(null);
  const [fileListLandscape, setFileListLandscape] = useState([]);
  const [fileListPortrait, setFileListPortrait] = useState([]);
  const [landscapeImageUrl, setLandscapeImageUrl] = useState("");
  const [portraitImageUrl, setPortraitImageUrl] = useState("");

  useEffect(() => {
    if (movieData) {
      form.setFieldsValue({
        ...movieData,
        release_date: dayjs(movieData.release_date, "YYYY-MM-DD"),
        end_date: dayjs(movieData.end_date, "YYYY-MM-DD"),
        genre_id: movieData.genres
          ? movieData.genres.map((genre) => genre.id)
          : [],
      });
      setReleaseData(
        movieData.release_date ? moment(movieData.release_date) : null
      );
      setEndData(movieData.end_date ? moment(movieData.end_date) : null);

      setLandscapeImageUrl(movieData.image_landscape?.url || "");
      setPortraitImageUrl(movieData.image_portrait?.url || "");

      if (movieData.image_landscape) {
        setFileListLandscape([
          {
            uid: "-1",
            name: "image_landscape.png",
            status: "done",
            url: movieData.image_landscape.url,
          },
        ]);
      }
      if (movieData.image_portrait) {
        setFileListPortrait([
          {
            uid: "-1",
            name: "image_portrait.png",
            status: "done",
            url: movieData.image_portrait.url,
          },
        ]);
      }
    }
  }, [movieData, form]);

  const { mutate, isPending } = useMovieMutation({
    action: "UPDATE",
    onSuccess: () => {
      messageApi.success("Thêm phim thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const genreOptions = useMemo(() => {
    if (!data || !data.genres) return [];
    return data.genres.map((genre) => ({
      value: genre.id,
      label: genre.name,
    }));
  }, [data]);

  const onFinish = (values) => {
    if (fileListLandscape.length > 0) {
      if (fileListLandscape[0].originFileObj) {
        values.image_landscape = fileListLandscape[0].thumbUrl;
      } else {
        values.image_landscape = {
          url: portraitImageUrl,
          public_id: movieData.image_portrait.public_id,
        };
      }
    }

    if (fileListPortrait.length > 0) {
      if (fileListPortrait[0].originFileObj) {
        values.image_portrait = fileListPortrait[0].thumbUrl;
      } else {
        values.image_portrait = {
          url: portraitImageUrl,
          public_id: movieData.image_portrait.public_id,
        };
      }
    }
    console.log(typeof values.image_portrait);
    // console.log({ ...values, id: id, release_date: releaseDate._i, end_date: endDate._i });
   mutate({ ...values, id: id, release_date: releaseDate._i, end_date: endDate._i });
  };

  return (
    <div>
      {contextHolder}
      <h1 className="text-2xl text-center">Cập nhật phim</h1>
      <Form
        form={form}
        layout="vertical"
        name="basic"
        style={{
          maxWidth: "100%",
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        autoComplete="on"
      >
        <Row gutter={16}>
          <Col span={18}>
            <Form.Item
              label="Nhập tên phim"
              name="name"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Thời lượng phim"
              name="duration"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={18}>
            <Form.Item
              label="Trailer"
              name="trailer"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Giới hạn độ tuổi"
              name="age_restriction"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={18}>
            <Form.Item
              label="Thể loại"
              name="genre_id"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Select
                mode="multiple"
                allowClear
                style={{
                  width: "100%",
                }}
                placeholder="Please select"
                options={genreOptions}
              />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Giá"
              name="price"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item
              label="Tác giả"
              name="director"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={8}>
            <Form.Item
              label="Diễn viên"
              name="actor"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item
              label="Ngày công chiếu"
              name="release_date"
              rules={[
                { required: true, message: "Không được bỏ trống" },
                { type: "date", message: "Ngày không hợp lệ" },
              ]}
            >
              <DatePicker
                className="w-full"
                format="YYYY-MM-DD"
                disabledDate={(current) => {
                  return current && current < dayjs().startOf("day");
                }}
                onChange={(date, dateString) => {
                  setReleaseData(dateString);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={5}>
            <Form.Item
              label="Ngày dừng chiếu"
              name="end_date"
              rules={[
                { required: true, message: "Không được bỏ trống" },
                { type: "date", message: "Ngày không hợp lệ" },
              ]}
            >
              <DatePicker
                className="w-full"
                format="YYYY-MM-DD"
                disabledDate={(current) => {
                  return current && current < dayjs().startOf("day");
                }}
                onChange={(date, dateString) => {
                  setEndData(dateString);
                }}
              />
            </Form.Item>
          </Col>
          <Col span={24}>
            <Form.Item
              label="Mô tả"
              name="description"
              rules={[{ required: true, message: "Không được bỏ trống" }]}
            >
              <Input />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="image_landscape"
              label="Ảnh ngang"
              rules={[
                {
                  required: !landscapeImageUrl,
                  message: "Vui lòng tải lên ảnh ngang",
                },
              ]}
            >
              <Upload
                listType="picture"
                fileList={fileListLandscape}
                beforeUpload={() => false}
                onChange={({ fileList }) => {
                  setFileListLandscape(fileList.slice(0, 1));
                }}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>Tải ảnh ngang mới</Button>
              </Upload>
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              name="image_portrait"
              label="Ảnh dọc"
              rules={[
                {
                  required: !portraitImageUrl,
                  message: "Vui lòng tải lên ảnh dọc",
                },
              ]}
            >
              <Upload
                listType="picture"
                fileList={fileListPortrait}
                beforeUpload={() => false}
                onChange={({ fileList }) => {
                  setFileListPortrait(fileList.slice(0, 1));
                }}
                maxCount={1}
              >
                <Button icon={<UploadOutlined />}>Tải ảnh dọc mới</Button>
              </Upload>
            </Form.Item>
          </Col>
        </Row>
        <Button key="submit" type="primary" onClick={() => form.submit()}>
          Lưu thay đổi
        </Button>
      </Form>
    </div>
  );
};

export default MovieUpdate;
