import React, { useState } from "react";
import { Button, Modal, Form, Input, message, Spin } from "antd";
import useGenreMutation from "../../hooks/Genre/useGenreMutation";

const GenreCreate = ({ open, onCancel }) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();
  const { mutate: createGenre, isPending } = useGenreMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Thêm danh mục thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const onFinish = (values) => {
    createGenre(values);
  };

  return (
    <>
      {contextHolder}
      <Modal
        title="Thêm thể loại phim"
        open={open}
        onCancel={isPending ? null : onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel} disabled={isPending}>
            Hủy
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => form.submit()}
            loading={isPending}
            disabled={isPending}
          >
            {isPending ? "Đang thêm..." : "Thêm"}
          </Button>,
        ]}
        maskClosable={!isPending}
        closable={!isPending}
      >
        <div style={{ position: "relative" }}>
          <Form
            form={form}
            name="basic"
            style={{
              maxWidth: 600,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              className="w-full"
              name="name"
              rules={[
                {
                  required: true,
                  message: "Vui lòng nhập tên thể loại!",
                },
              ]}
            >
              <Input disabled={isPending} />
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </>
  );
};

export default GenreCreate;
