import React, { useEffect, useState } from "react";
import { Button, Modal, Form, Input, message } from "antd";
import useGenreMutation from "../../hooks/Genre/useGenreMutation";
const GenreUpdate = ({ open, onCancel, genre }) => {
  const [form] = Form.useForm();
  useEffect(() => {
    if (genre) {
      form.setFieldsValue({
        name: genre.name,
      });
    }
  }, [genre, form]);
  const [messageApi, contextHolder] = message.useMessage();
  const { mutate: updateGenre, isPending } = useGenreMutation({
    action: "UPDATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Cập nhật danh mục thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });
  const onFinish =  (values) => {
    const genreData = {
      name: values.name,
      id: genre.id,
    };
     updateGenre(genreData);
  };
  const handleSubmit = () => {
    form.submit();
  };
  return (
    <>
      {contextHolder}
      <Modal
        title="Sửa thể loại phim"
        open={open}
        onCancel={onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" onClick={handleSubmit}>
            {isPending ? "Đang thêm" : "Lưu"}
          </Button>,
        ]}
      >
        <Form
          form={form}
          name="basic"
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            className="w-full"
            name="name"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default GenreUpdate;
