import { Button, Space, Table, message } from "antd";
import React, { useState } from "react";
import useGenreQuery from "../../hooks/Genre/useGenreQuery";
import { formatDate } from "../../systems/utils/formatDate";
import GenreCreate from "./GenreCreate";
import GenreUpdate from "./GenreUpdate";
import { EditOutlined } from "@ant-design/icons";

const columns = (openModelUpdate) => [
  {
    title: "Thể loại",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày chỉnh sửa",
    dataIndex: "updatedAt",
    key: "updatedAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Chức năng",
    key: "action",
    render: (_, genre) => (
      <Button type="primary" ghost onClick={() => openModelUpdate(genre)}>
        <EditOutlined />
      </Button>
    ),
  },
];

const GenreList = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useGenreQuery();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedGenre, setSelectedGenre] = useState(null);
  const [modalType, setModalType] = useState(null);

  const dataSource = data?.genres?.map((item) => ({
    key: item.id,
    ...item,
  }));
  const showModal = () => {
    setIsModalOpen(true);
    setModalType("create");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setSelectedGenre(null);
  };

  const openModelUpdate = (genre) => {
    setSelectedGenre(genre);
    setIsModalOpen(true);
    setModalType("update");
  };

  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      {contextHolder}
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Thể loại phim</h1>
        <Button type="primary" onClick={showModal}>
          Thêm thể loại
        </Button>
      </div>
      {isModalOpen && modalType === "create" && (
        <GenreCreate open={isModalOpen} onCancel={handleCancel} />
      )}
      {isModalOpen && modalType === "update" && selectedGenre && (
        <GenreUpdate
          open={isModalOpen}
          onCancel={handleCancel}
          genre={selectedGenre}
        />
      )}
      <Table
        columns={columns(openModelUpdate)}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default GenreList;
