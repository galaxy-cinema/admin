import { CloseOutlined } from "@ant-design/icons";
import { Button, Select, Space, Spin } from "antd";
import React, { useEffect, useState } from "react";
import useSeatTypeQuery from "../../hooks/SeatType/useSeatTypeQuery";
import useSeatMutation from "../../hooks/Seat/useSeatMutation";
import { update } from "../../services/seat";
const optionsStatusSeat = [
  { value: "Available", label: "Available" },
  { value: "Maintenance", label: "Maintenance" },
];

const SeatMap = ({ seatData, refetchRoom }) => {
  const [dataSeat, setDataSeat] = useState([]);
  const [selectedSeats, setSelectedSeats] = useState([]);
  const [selectedSeatType, setSelectedSeatType] = useState(null);
  const [selectedStatus, setSelectedStatus] = useState("Available");
  const { data: seatTypeData, isLoading } = useSeatTypeQuery();
  const { mutate, isPending } = useSeatMutation({
    action: "UPDATE",
    onSuccess: () => {
      messageApi.success("Cập nhật ghế thành công");
      setTimeout(() => {
        onCancel();
      }, 1000);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });
  useEffect(() => {
    if (seatData) setDataSeat(seatData.map((s) => s));
  }, [seatData]);

  const groupedSeats = dataSeat.reduce((acc, seat) => {
    if (!acc[seat.name]) {
      acc[seat.name] = [];
    }
    acc[seat.name].push(seat);
    return acc;
  }, {});
  const sortedGroupedSeats = Object.keys(groupedSeats)
    .sort()
    .reduce((acc, key) => {
      acc[key] = groupedSeats[key].sort((a, b) => a.column - b.column);
      return acc;
    }, {});
  const maxRowLength = Math.max(
    ...Object.values(groupedSeats).map((seats) => seats.length)
  );
  const handleSeatClick = (seat) => {
    setSelectedSeats((prevSelectedSeats) => {
      if (prevSelectedSeats.some((s) => s.id === seat.id)) {
        return prevSelectedSeats.filter((s) => s.id !== seat.id);
      } else {
        return [...prevSelectedSeats, seat];
      }
    });
  };
  const handleSave = async () => {
    try {
      const updatedSeats = selectedSeats.map((seat) => ({
        ...seat,
        seat_type_id: selectedSeatType,
        status: selectedStatus,
      }));
      mutate(updatedSeats);
      refetchRoom();
      setSelectedSeats([]);
    } catch (error) {
      message.error("Cập nhật ghế thất bại");
    }
  };

  const isSeatSelected = (seatId) => {
    return selectedSeats.some((seat) => seat.id === seatId);
  };

  const getSeatColor = (seat) => {
    if (isSeatSelected(seat.id)) {
      return "bg-blue-500 text-white";
    } else if (seat.status !== "Available") {
      return "bg-red-200";
    } else if (seat.seat_type.name === "VIP") {
      return "bg-orange-600";
    }
    return "";
  };

  return (
    <>
      {isLoading ? (
        <Spin />
      ) : (
        <div>
          <div className="seat-map">
            {selectedSeats.length > 0 && (
              <div className="mb-4 flex flex-row justify-between animate-fade">
                <Space>
                  <button onClick={() => setSelectedSeats([])}>
                    <CloseOutlined />
                  </button>
                  <p className="ml-2">Số ghế đã chọn {selectedSeats.length}</p>
                </Space>
                <Space
                  direction="vertical"
                  size="middle"
                  className="flex flex-row"
                >
                  <Space>
                    <div>Kiểu ghế:</div>
                    <Select
                      size="middle"
                      style={{ width: 200 }}
                      value={selectedSeatType}
                      onChange={(value) => setSelectedSeatType(value)}
                      options={seatTypeData.seat_types.map((type) => ({
                        value: type.id,
                        label: type.name,
                      }))}
                    />
                  </Space>
                  <Space>
                    <div>Trạng thái ghế:</div>
                    <Select
                      size="middle"
                      options={optionsStatusSeat}
                      style={{ width: 200 }}
                      value={selectedStatus}
                      onChange={(value) => setSelectedStatus(value)}
                    />
                  </Space>
                </Space>
                <Button
                  type="primary"
                  onClick={handleSave}
                  disabled={selectedSeats.length === 0}
                >
                  Lưu thay đổi
                </Button>
              </div>
            )}
            <div className="p-4">
              {Object.keys(sortedGroupedSeats).map((group) => (
                <div
                  key={group}
                  className="mb-4 flex items-center justify-between"
                >
                  <h2 className="text-lg font-bold"> {group}</h2>
                  <div
                    style={{
                      gridTemplateColumns: `repeat(${maxRowLength}, 1fr)`,
                    }}
                    className="grid gap-2"
                  >
                    {groupedSeats[group].map((seat) => (
                      <div key={seat.id} className="relative">
                        <Button
                          className={`py-1 px-3 border rounded-sm ${getSeatColor(
                            seat
                          )}`}
                          onClick={() => handleSeatClick(seat)}
                          disabled={seat.status !== "Available"}
                        >
                          {seat.column}
                        </Button>
                      </div>
                    ))}
                  </div>
                  <h2 className="text-lg font-bold"> {group}</h2>
                </div>
              ))}
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export default SeatMap;
