import { EditOutlined } from "@ant-design/icons";
import { Button, Table } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useCinemaQuery from "../../hooks/Cinema/useCinemaQuery";
import { formatDate } from "../../systems/utils/formatDate";
import CinemaCreate from "./CinemaCreate";
import CinemaUpdate from "./CinemaUpdate";
const columns = (openModelUpdate) => [
  {
    title: "Rạp phim",
    dataIndex: "name",
    key: "name",
    render: (text, cinema) => <Link to={`/cinemas/${cinema.id}`}>{text}</Link>,
  },
  {
    title: "Địa chỉ",
    dataIndex: "location",
    key: "location",
  },
  {
    title: "Số điện thoại",
    dataIndex: "phone",
    key: "phone",
  },
  {
    title: "Email",
    dataIndex: "email",
    key: "email",
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày chỉnh sửa",
    dataIndex: "updatedAt",
    key: "updatedAt",
    render: (date) => formatDate(date),
  },
  {
    title: "",
    key: "action",
    render: (_, cinema) => (
      <Button type="primary" ghost onClick={() => openModelUpdate(cinema)}>
        <EditOutlined />
      </Button>
    ),
  },
];

const CinemaList = () => {
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useCinemaQuery();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedCinema, setSelectedCinema] = useState(null);
  const [modalType, setModalType] = useState(null);
  const dataSource = data?.cinemas?.map((item) => ({
    key: item.id,
    ...item,
  }));

  const showModal = () => {
    setIsModalOpen(true);
    setModalType("create");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setSelectedCinema(null);
  };

  const openModelUpdate = (cinema) => {
    setSelectedCinema(cinema);
    setIsModalOpen(true);
    setModalType("update");
  };

  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Danh sách rạp chiếu phim</h1>
        <Button type="primary" onClick={showModal}>
          Thêm rạp
        </Button>
      </div>
      {isModalOpen && modalType === "create" && (
        <CinemaCreate open={isModalOpen} onCancel={handleCancel} />
      )}
      {isModalOpen && modalType === "update" && selectedCinema && (
        <CinemaUpdate
          open={isModalOpen}
          onCancel={handleCancel}
          cinema={selectedCinema}
        />
      )}
      <Table
        columns={columns(openModelUpdate)}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default CinemaList;
