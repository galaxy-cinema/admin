import {
  Button,
  Col,
  DatePicker,
  Form,
  Input,
  message,
  Modal,
  Row,
} from "antd";
import React from "react";
import useCinemaMutation from "../../hooks/Cinema/useCinemaMutation";

const CinemaCreate = ({ open, onCancel }) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();
  const { mutate: create, isPending } = useCinemaMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Thêm rạp thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const onFinish = (values) => {
    create(values);
    console.log(values);
  };

  return (
    <>
      {contextHolder}
      <Modal
        title="Thêm rạp chiếu phim"
        open={open}
        onCancel={isPending ? null : onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel} disabled={isPending}>
            Hủy
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => form.submit()}
            loading={isPending}
            disabled={isPending}
          >
            {isPending ? "Đang thêm..." : "Thêm"}
          </Button>,
        ]}
        maskClosable={!isPending}
        closable={!isPending}
      >
        <div>
          <Form
            form={form}
            name="basic"
            style={{
              maxWidth: "100%",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="off"
          >
            <Form.Item
              label="Tên rạp"
              name="name"
              rules={[{ required: true, message: "Vui lòng nhập tên rạp" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Địa chỉ"
              name="location"
              rules={[{ required: true, message: "Vui lòng nhập địa chỉ" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Hotline"
              name="phone"
              rules={[
                {
                  validator: (_, value) => {
                    if (!value) {
                      return Promise.reject("Vui lòng nhập số hotline");
                    }
                    if (!/^\d+$/.test(value)) {
                      return Promise.reject("Sai định dạng");
                    }
                    return Promise.resolve();
                  },
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Email"
              name="email"
              rules={[
                { required: true, message: "Vui lòng nhập địa chỉ email" },
                { type: "email", message: "Sai định dạng" },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </>
  );
};

export default CinemaCreate;
