import React, { useEffect, useState } from "react";
import { Button, Modal, Form, Input, message } from "antd";
import { validateFieldNumber } from "../../systems/utils/validateFieldNumber";
import useSeatTypeMutation from "../../hooks/SeatType/useSeatTypeMutation";
const SeatTypeUpdate = ({ open, onCancel, seatType }) => {
  const [form] = Form.useForm();
  useEffect(() => {
    if (seatType) {
      form.setFieldsValue({
        name: seatType.name,
        price: seatType.price,
      });
    }
  }, [seatType, form]);
  const [messageApi, contextHolder] = message.useMessage();
  const { mutate, isPending } = useSeatTypeMutation({
    action: "UPDATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Cập nhật loại ghế thành công");
      setTimeout(() => {
        onCancel();
      }, [1000]);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });
  const onFinish =  (values) => {
    const seatTypeData = {
      name: values.name,
      price: values.price,
      id: seatType.id,
    };
     mutate(seatTypeData);
  };
  const handleSubmit = () => {
    form.submit();
  };
  return (
    <>
      {contextHolder}
      <Modal
        title="Sửa thể loại phim"
        open={open}
        onCancel={onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel}>
            Hủy
          </Button>,
          <Button key="submit" type="primary" onClick={handleSubmit}>
            {isPending ? "Đang thêm" : "Lưu"}
          </Button>,
        ]}
      >
        <Form
          form={form}
          name="basic"
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="off"
        >
          <Form.Item
            label="Tên"
            className="w-full"
            name="name"
            rules={[
              {
                required: true,
                message: "Vui lòng nhập tên loại ghế",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Giá"
            className="w-full"
            name="price"
            rules={[
              {
                validator: (_, value) => validateFieldNumber("giá", value),
              },
            ]}
          >
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
export default SeatTypeUpdate;
