import { Button, Form, Input, message, Modal } from "antd";
import React from "react";
import useSeatTypeMutation from "../../hooks/SeatType/useSeatTypeMutation";
import { validateFieldNumber } from "../../systems/utils/validateFieldNumber";

const SeatTypeCreate = ({ open, onCancel }) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();

  const { mutate, isPending } = useSeatTypeMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Thêm loại ghế thành công");
      setTimeout(() => {
        onCancel();
      }, 1000);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const onFinish = (values) => {
    mutate(values);
  };
  return (
    <>
      {contextHolder}
      <Modal
        title="Thêm loại ghế"
        open={open}
        onCancel={isPending ? null : onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel} disabled={isPending}>
            Hủy
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => form.submit()}
            loading={isPending}
            disabled={isPending}
          >
            {isPending ? "Đang thêm..." : "Thêm"}
          </Button>,
        ]}
        maskClosable={!isPending}
        closable={!isPending}
      >
        <div>
          <Form
            form={form}
            name="basic"
            style={{
              maxWidth: "100%",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="on"
          >
            <Form.Item
              label="Tên"
              name="name"
              rules={[
                { required: true, message: "Vui lòng nhập tên loại ghế" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Giá "
              name="price"
              rules={[
                {
                  validator: (_, value) => validateFieldNumber("giá", value),
                },
              ]}
            >
              <Input />
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </>
  );
};

export default SeatTypeCreate;
