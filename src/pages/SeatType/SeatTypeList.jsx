import { Button, Space, Table, message } from "antd";
import React, { useState } from "react";
import { formatDate } from "../../systems/utils/formatDate";
import SeatTypeCreate from "./SeatTypeCreate";
import SeatTypeUpdate from "./SeatTypeUpdate";
import useSeatTypeQuery from "../../hooks/SeatType/useSeatTypeQuery";
import { EditOutlined } from "@ant-design/icons";

const columns = (openModelUpdate) => [
  {
    title: "Loại ghế",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Giá",
    dataIndex: "price",
    key: "price",
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày chỉnh sửa",
    dataIndex: "updatedAt",
    key: "updatedAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Chức năng",
    key: "action",
    render: (_, seatType) => (
      <Button type="primary" ghost onClick={() => openModelUpdate(seatType)}>
        <EditOutlined />
      </Button>
    ),
  },
];

const SeatTypeList = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useSeatTypeQuery();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedSeatType, setSelectedSeatType] = useState(null);
  const [modalType, setModalType] = useState(null);

  const dataSource = data?.seat_types?.map((item) => ({
    key: item.id,
    ...item,
  }));
  const showModal = () => {
    setIsModalOpen(true);
    setModalType("create");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setSelectedSeatType(null);
  };

  const openModelUpdate = (seatType) => {
    setSelectedSeatType(seatType);
    setIsModalOpen(true);
    setModalType("update");
  };

  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      {contextHolder}
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Danh sách loại ghế</h1>
        <Button type="primary" onClick={showModal}>
          Thêm loại ghế
        </Button>
      </div>
      {isModalOpen && modalType === "create" && (
        <SeatTypeCreate open={isModalOpen} onCancel={handleCancel} />
      )}
      {isModalOpen && modalType === "update" && selectedSeatType && (
        <SeatTypeUpdate
          open={isModalOpen}
          onCancel={handleCancel}
          seatType={selectedSeatType}
        />
      )}
      <Table
        columns={columns(openModelUpdate)}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default SeatTypeList;
