import { Button, Select, Form, Input, message, Modal, InputNumber } from "antd";
import React, { useMemo } from "react";
import useRoomMutation from "../../hooks/Room/useRoomMutation";
import useCinemaQuery from "../../hooks/Cinema/useCinemaQuery";
import { validateFieldNumber } from "../../systems/utils/validateFieldNumber";

const RoomCreate = ({ open, onCancel }) => {
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();
  const { data, isLoading } = useCinemaQuery();

  const cinemaOptions = useMemo(() => {
    if (!data || !data.cinemas) return [];
    return data.cinemas.map((cinema) => ({
      value: cinema.id,
      label: cinema.name,
    }));
  }, [data]);

  const { mutate, isPending } = useRoomMutation({
    action: "CREATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Thêm rạp thành công");
      setTimeout(() => {
        onCancel();
      }, 1000);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const onFinish = (values) => {
    mutate(values);
  };

  const handleValuesChange = (_, allValues) => {
    const { total_row, total_column } = allValues;
    if (total_row && total_column) {
      const capacity = total_row * total_column;
      form.setFieldsValue({ capacity });
    }
  };

  return (
    <>
      {contextHolder}
      <Modal
        title="Thêm phòng chiếu phim"
        open={open}
        onCancel={isPending ? null : onCancel}
        footer={[
          <Button key="cancel" onClick={onCancel} disabled={isPending}>
            Hủy
          </Button>,
          <Button
            key="submit"
            type="primary"
            onClick={() => form.submit()}
            loading={isPending}
            disabled={isPending}
          >
            {isPending ? "Đang thêm..." : "Thêm"}
          </Button>,
        ]}
        maskClosable={!isPending}
        closable={!isPending}
      >
        <div>
          <Form
            form={form}
            name="basic"
            style={{
              maxWidth: "100%",
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            autoComplete="on"
            onValuesChange={handleValuesChange}
          >
            <Form.Item
              label="Chọn rạp"
              name="cinema_id"
              rules={[{ required: true, message: "Vui lòng chọn rạp" }]}
            >
              <Select
                loading={isLoading}
                placeholder="Chọn rạp chiếu"
                style={{
                  width: "100%",
                }}
                options={cinemaOptions}
              />
            </Form.Item>
            <Form.Item
              label="Tên phòng chiếu"
              name="name"
              rules={[
                { required: true, message: "Vui lòng nhập tên phòng chiếu" },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Số cột"
              name="total_row"
              rules={[
                {
                  validator: (_, value) => validateFieldNumber("cột", value),
                },
              ]}
            >
              <InputNumber className="w-full" />
            </Form.Item>
            <Form.Item
              label="Số hàng"
              name="total_column"
              rules={[
                {
                  validator: (_, value) => validateFieldNumber("hàng", value),
                },
              ]}
            >
              <InputNumber className="w-full" />
            </Form.Item>
            <Form.Item label="Tổng số lượng ghế" name="capacity">
              <Input disabled />
            </Form.Item>
          </Form>
        </div>
      </Modal>
    </>
  );
};

export default RoomCreate;
