import { Col, Form, Input, InputNumber, message, Row, Select } from "antd";
import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import useRoomMutation from "../../hooks/Room/useRoomMutation";
import useRoomQuery from "../../hooks/Room/useRoomQuery";
import { validateFieldNumber } from "../../systems/utils/validateFieldNumber";
import SeatMap from "../Seat/SeatMap";
const RoomUpdate = () => {
  const { id } = useParams();
  const [messageApi, contextHolder] = message.useMessage();
  const [form] = Form.useForm();
  const { data: roomData, isLoading, refetch } = useRoomQuery(id);
  useEffect(() => {
    if (roomData && roomData.room && roomData.room) {
      const room = roomData.room;
      form.setFieldsValue({
        name: room.name,
        total_column: room.total_column,
        total_row: room.total_row,
        capacity: room.capacity,
        cinema_id: room.cinema_id,
      });
    }
  }, [roomData, form]);
  const { mutate, isPending } = useRoomMutation({
    action: "UPDATE",
    onSuccess: () => {
      form.resetFields();
      messageApi.success("Cập nhật phòng chiếu thành công");
      setTimeout(() => {
        onCancel();
      }, 1000);
    },
    onError: (error) => {
      messageApi.error(`${error.response.data.message}`);
    },
  });

  const onFinish = (values) => {
    const updatedValues = { ...values, id: room.id };
    mutate(updatedValues);
  };

  const handleValuesChange = (_, allValues) => {
    const { total_row, total_column } = allValues;
    if (total_row && total_column) {
      const capacity = total_row * total_column;
      form.setFieldsValue({ capacity });
    }
  };

  return (
    <>
      {contextHolder}
      <div>
        <h1 className="text-3xl text-center mb-8">
          Chỉnh sửa phòng chiếu {roomData?.room?.name}
        </h1>
        <Form
          form={form}
          name="basic"
          layout="vertical"
          style={{
            maxWidth: "100%",
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          autoComplete="on"
          onValuesChange={handleValuesChange}
        >
          <Row gutter={16}>
            <Col span={12}>
              <Form.Item
                label="Chọn rạp"
                name="cinema_id"
                rules={[{ required: true, message: "Vui lòng chọn rạp" }]}
              >
                <Select disabled placeholder="Chọn rạp chiếu" />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                label="Tên phòng chiếu"
                name="name"
                rules={[
                  { required: true, message: "Vui lòng nhập tên phòng chiếu" },
                ]}
              >
                <Input />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                label="Số cột"
                name="total_row"
                rules={[
                  {
                    validator: (_, value) => validateFieldNumber("cột", value),
                  },
                ]}
              >
                <InputNumber className="w-full" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item
                label="Số hàng"
                name="total_column"
                rules={[
                  {
                    validator: (_, value) => validateFieldNumber("hàng", value),
                  },
                ]}
              >
                <InputNumber className="w-full" />
              </Form.Item>
            </Col>
            <Col span={8}>
              <Form.Item label="Tổng số lượng ghế" name="capacity">
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
        </Form>
        {roomData?.room?.id && (
          <SeatMap
            seatData={roomData.room.seats}
            roomId={roomData.room.id}
            refetchRoom={refetch}
          />
        )}
      </div>
    </>
  );
};

export default RoomUpdate;
