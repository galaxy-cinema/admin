import { EditOutlined } from "@ant-design/icons";
import { Button, Table, message } from "antd";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import useRoomQuery from "../../hooks/Room/useRoomQuery";
import { formatDate } from "../../systems/utils/formatDate";
import RoomCreate from "./RoomCreate";
import RoomUpdate from "./RoomUpdate";

const columns = () => [
  {
    title: "Tên phòng chiếu",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Tên rạp",
    dataIndex: "cinema",
    key: "cinema",
    render: (text) => text.name,
  },
  {
    title: "Số lượng ghế (tối đa)",
    dataIndex: "capacity",
    key: "capacity",
  },
  {
    title: "Ngày tạo",
    dataIndex: "createdAt",
    key: "createdAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Ngày chỉnh sửa",
    dataIndex: "updatedAt",
    key: "updatedAt",
    render: (date) => formatDate(date),
  },
  {
    title: "Chức năng",
    key: "action",
    render: (_, room) => (
      <>
        <Link to={`/rooms/${room.id}`}>
          <EditOutlined />
        </Link>
      </>
    ),
  },
];

const RoomList = () => {
  const [messageApi, contextHolder] = message.useMessage();
  const [currentPage, setCurrentPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, isLoading } = useRoomQuery();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [selectedRoom, setSelectedRoom] = useState(null);
  const [modalType, setModalType] = useState(null);

  const dataSource = data?.rooms?.map((item) => ({
    key: item.id,
    ...item,
  }));
  console.log(dataSource);
  const showModal = () => {
    setIsModalOpen(true);
    setModalType("create");
  };

  const handleCancel = () => {
    setIsModalOpen(false);
    setSelectedRoom(null);
  };

  const openModelUpdate = (room) => {
    setSelectedRoom(room);
    setIsModalOpen(true);
    setModalType("update");
  };

  const handleTableChange = (pagination) => {
    setCurrentPage(pagination.current);
    setPageSize(pagination.pageSize);
  };

  return (
    <div>
      {contextHolder}
      <div className="flex items-center justify-between">
        <h1 className="text-2xl mb-3">Danh sách phòng chiếu phim</h1>
        <Button type="primary" onClick={showModal}>
          Thêm phòng chiếu
        </Button>
      </div>
      {isModalOpen && modalType === "create" && (
        <RoomCreate open={isModalOpen} onCancel={handleCancel} />
      )}
      {isModalOpen && modalType === "update" && selectedRoom && (
        <RoomUpdate
          open={isModalOpen}
          onCancel={handleCancel}
          room={selectedRoom}
        />
      )}
      <Table
        columns={columns(openModelUpdate)}
        dataSource={dataSource}
        loading={isLoading}
        pagination={{
          current: currentPage,
          pageSize: pageSize,
          total: data?.totalItems,
          showTotal: (total) => `Tổng ${total} mục`,
        }}
        onChange={handleTableChange}
      />
    </div>
  );
};

export default RoomList;
