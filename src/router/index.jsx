import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import Login from "../pages/Login/Login";
import BasicLayout from "../layout/BasicLayout";
import NotFound from "../pages/NotFound/NotFound";
import GenreList from "../pages/Genre/GenreList";
import CinemaCreate from "../pages/Cinema/CinemaCreate";
import CinemaList from "../pages/Cinema/CinemaList";
import ProtectedRoute from "./protectedRoute.jsx";
import Dashboard from "../pages/Dashboard/Dashboard.jsx";
import RoomList from "../pages/Room/RoomList.jsx";
import SeatTypeList from "../pages/SeatType/SeatTypeList.jsx";
import RoomUpdate from "../pages/Room/RoomUpdate.jsx";
import MovieList from "../pages/Movie/MovieList.jsx";
import MovieCreate from "../pages/Movie/MovieCreate.jsx";
import MovieUpdate from "../pages/Movie/MovieUpdate.jsx";
import ScheduleList from "../pages/Schedule/ScheduleList.jsx";

const RouterComponent = () => {
  return (
    <div>
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route element={<ProtectedRoute />}>
            <Route path="/" element={<BasicLayout />}>
              <Route index element={<Dashboard />} />
              <Route path="/genres" element={<GenreList />} />
              <Route path="/cinemas" element={<CinemaList />} />
              <Route path="/schedules" element={<ScheduleList />} />
              <Route path="/rooms" element={<RoomList />} />
              <Route path="/rooms/:id" element={<RoomUpdate />} />
              <Route path="/seat-types" element={<SeatTypeList />} />
              <Route path="/movies" element={<MovieList />} />
              <Route path="/movies/:id" element={<MovieUpdate />} />
              <Route path="/movies/create" element={<MovieCreate />} />
            </Route>
          </Route>
          <Route path="*" element={<NotFound />} />
        </Routes>
      </Router>
    </div>
  );
};

export default RouterComponent;
