import React from "react";
import { Navigate, Outlet } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import { logout } from "../services/auth";

const protectedRoute = () => {
  const token = localStorage.getItem("authToken");

  if (token) {
    try {
      const decodedToken = jwtDecode(token);
      if (!decodedToken) {
        logout();
        console.log("Token không hợp lệ");
        localStorage.removeItem("authToken");
        return <Navigate to="/login" replace />;
      }
    } catch (error) {
      logout();
      console.error("Lỗi giải mã token:", error);
      localStorage.removeItem("authToken");
      return <Navigate to="/login" replace />;
    }
  } else {
    return <Navigate to="/login" replace />;
  }

  return <Outlet />;
};

export default protectedRoute;
