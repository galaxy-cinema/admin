import {
  AppstoreOutlined,
  MailOutlined,
  SettingOutlined,
  YoutubeOutlined,
} from "@ant-design/icons";
import { NavLink } from "react-router-dom";
const items = [
  {
    key: "1",
    label: "Thống kê",
    children: [
      {
        key: "13",
        label: "Option 3",
      },
      {
        key: "14",
        label: "Option 4",
      },
    ],
  },
  {
    key: "2",
    label: <NavLink to={"/cinemas"}>Quản lý rạp</NavLink>,
  },
  {
    key: "6",
    label: "Quản lý phim",
    children: [
      {
        key: "10",
        label: <NavLink to={"/movies"}>Danh sách phim</NavLink>,
      },
      {
        key: "11",
        label: <NavLink to={"/movies/create"}>Tạo phim</NavLink>,
      },
    ],
  },
  {
    key: "7",
    label: <NavLink to={"/schedules"}>Quản lý lịch chiếu</NavLink>,
  },
  {
    key: "3",
    label: <NavLink to={"/genres"}>Quản lý thể loại</NavLink>,
  },
  {
    key: "4",
    label: <NavLink to={"/rooms"}>Quản lý phòng chiếu</NavLink>,
  },
  {
    key: "5",
    label: <NavLink to={"/seat-types"}>Quản lý loại ghế</NavLink>,
  },
];
export default items;
